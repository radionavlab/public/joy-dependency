#ifndef JOY_DRIVERS_MG_H
#define JOY_DRIVERS_MG_H

#include "joyStructs.h"
#include "sensor_msgs/Joy.h"

// Driver for receiving data from a wired Xbox 360 controller
joyStruct driverXbox360Wired(const sensor_msgs::Joy& msg);

// Function to print values received from joystick (for debug purposes)
void printJoyValues(const joyStruct& joy);

#endif
