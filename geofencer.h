#include <Eigen/Core>

class GeoFencer {

public:
  void initialize(const Eigen::Vector2d boundsX,
                  const Eigen::Vector2d boundsY,
                  const Eigen::Vector2d boundsZ,
                  const Eigen::Vector2d bounds_x,
                  const Eigen::Vector2d bounds_y,
                  const Eigen::Vector2d bounds_z,
                  const Eigen::Matrix3d RCI,
                  const Eigen::Vector3d rotCenter,
                  const bool GeoFlag);

  // Given a set of bounds, saturates val to the bounds and returns it.
  double saturate(double val, const Eigen::Vector2d hard_bounds);

  // Given a set of bounds, saturates val to the bounds and returns it.
  double pos_soft_sat(double val_pos, const Eigen::Vector2d soft_bounds, const Eigen::Vector2d hard_bounds, double ini_vel,
                      double time);
  double vel_soft_sat(double val_pos, double vel_max, const Eigen::Vector2d soft_bounds, const Eigen::Vector2d hard_bounds);
  double acc_soft_sat(double val_pos, const Eigen::Vector2d soft_bounds);


  // Given a set of bounds, saturates val to the bounds and returns it.
  double joy_saturate(double val, const Eigen::Vector2d bounds);

    // Given a set of bounds, saturates val to just outside the bounds and returns it.
  double vel_acc_saturate(double val, double val_pos, const Eigen::Vector2d bounds);

  // Returns true if the value violates given bounds.
  double violates(double val, const Eigen::Vector2d bounds);

  // Operates on pos, vel, and acceleration inputs in-place to enforce a hard geofence.
  void geofencePVA(Eigen::Vector3d &pos, Eigen::Vector3d &vel, Eigen::Vector3d &acc, double dt);

  // Operates on pos, vel, and acceleration inputs in-place to enforce a soft geofence.
  void geofencePVA_soft(Eigen::Vector3d &pos, Eigen::Vector3d &vel, Eigen::Vector3d &acc);


  // Geofencing constants and functions. Constraints given as a single rotation matrix into the
  // constraint frame Co from inertial frame I, then given as simple x/y/z upper/lower bounds in
  // Co-space. Co is named to deconflict from camera frame C.
  Eigen::Vector2d X_bounds;
  Eigen::Vector2d Y_bounds;
  Eigen::Vector2d Z_bounds;
  Eigen::Vector2d x_bounds;
  Eigen::Vector2d y_bounds;
  Eigen::Vector2d z_bounds;
  Eigen::Matrix3d _RCI;
  Eigen::Vector3d rot_C;
  bool geo_flag;
};



