#include <Eigen/Core>

class buffer {

public:
  void initialize(const double dist_short,
                  const double dist_long,
                  const bool Buff_Flag);

  Eigen::Vector3d saturate(Eigen::Vector3d our_pos, Eigen::Vector3d their_pos, double short_dist);

  bool violates(Eigen::Vector3d our_pos, Eigen::Vector3d their_pos, double short_dist);

  Eigen::Vector3d unit_vector(Eigen::Vector3d our_pos, Eigen::Vector3d their_pos);


  double short_dist;
  double long_dist;
  Eigen::Matrix3d _RCI;
  Eigen::Vector3d enemy_pos;
  Eigen::Vector3d enemy_vel;
  bool buff_flag;
};
