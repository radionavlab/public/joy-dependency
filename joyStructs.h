#ifndef __JOY_STRUCTS_H
#define __JOY_STRUCTS_H

// Incoming data from joysticks
struct joyStruct {
  // Message sequence number
  int seq;           
  int buttonA;
  int buttonB;
  int buttonX;
  int buttonY;
  int buttonR1;
  int buttonL1;
  int buttonSelect;
  int buttonStart;
  int buttonLeft;
  int buttonRight;
  int buttonUp;
  int buttonDown;

  double LstickHor;
  double LstickVer;
  double RstickHor;
  double RstickVer;
  double L2;
  double R2;
};

// Joy Parameters
struct JoyParams {
  // All angles in degrees and angular rates in degrees/sec
  double roll_max = 40;
  double pitch_max = 40;
  double yaw_rate_max = 60;
  // Normalized value: ranges from 0 to 1
  double thrust_max = 1;
  // Horizontal (x and y) and vertical (z) speeds in meters/sec
  double x_rate = 3;
  double y_rate = 3;
  double z_rate = 2;
  // Horizontal (x and y) and vertical (z) speeds in meters/sec
  double x_rate_demo = 0.75;
  double y_rate_demo = 0.75;
  double z_rate_demo = 0.50;
  // Horizontal filter parameters
  double Bn_horizontal_Hz = 1;
  double zeta_horizontal = 2;
  // Vertical filter parameters
  double Bn_vertical_Hz = 1;
  double zeta_vertical = 2;
};

// Initialize values for the joy structure
void initializeJoy(joyStruct& Joy);

#endif
