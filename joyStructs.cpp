#include "joyStructs.h"

// The values below are meant to be the resting state values of the various
// buttons.
void initializeJoy(joyStruct& Joy) {
  Joy.seq = 0;
  Joy.buttonA = 0;
  Joy.buttonB = 0;
  Joy.buttonX = 0;
  Joy.buttonY = 0;
  Joy.buttonR1 = 0;
  Joy.buttonL1 = 0;
  Joy.buttonSelect = 0;
  Joy.buttonStart = 0;
  Joy.buttonLeft = 0;
  Joy.buttonRight = 0;
  Joy.buttonUp = 0;
  Joy.buttonDown = 0;

  Joy.LstickHor = 0;
  Joy.LstickVer = 0;
  Joy.RstickHor = 0;
  Joy.RstickVer = 0;
  Joy.L2 = 1;
  Joy.R2 = 1;
}
