#include "buffer.h"
#include <Eigen/Core>



void buffer::initialize(const double dist_short,
                        const double dist_long,
                        const bool Buff_Flag) {

  short_dist = dist_short;
  long_dist = dist_long;
  enemy_pos = {0,0,0};
  enemy_vel = {0,0,0};
  buff_flag = Buff_Flag;  
}

bool buffer::violates(Eigen::Vector3d our_pos, Eigen::Vector3d their_pos, double short_dist) {
  Eigen::Vector3d pointer = our_pos-their_pos;
  Eigen::Vector3d buff_vec = short_dist*(unit_vector(our_pos, their_pos));
  if(pointer.norm()<buff_vec.norm()){
    return true;
  }
  else{
    return false;
  }
}

Eigen::Vector3d buffer::unit_vector(Eigen::Vector3d our_pos, Eigen::Vector3d their_pos) {
  Eigen::Vector3d pointer = our_pos-their_pos;
  double mag = pointer.norm();
  Eigen::Vector3d unit_vec = pointer/mag;
  return unit_vec;
}

Eigen::Vector3d buffer::saturate(Eigen::Vector3d our_pos, Eigen::Vector3d their_pos, double short_dist) {
  Eigen::Vector3d buff_vec = short_dist*(unit_vector(our_pos, their_pos));
  Eigen::Vector3d final_vec = their_pos+buff_vec;
  return final_vec;
}
