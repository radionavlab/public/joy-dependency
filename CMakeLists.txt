cmake_minimum_required(VERSION 3.1)

set(TARGET lib_joy)

find_package (Eigen3 3.3 REQUIRED NO_MODULE)

find_package(catkin REQUIRED COMPONENTS
  sensor_msgs
  )

set(SOURCE_FILES
  buffer.cpp
  joyDrivers.cpp
  joyMapper.cpp
  joyStructs.cpp
  geofencer.cpp
)

add_library(${TARGET} STATIC ${SOURCE_FILES})


target_include_directories(${TARGET} PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${catkin_INCLUDE_DIRS}
)


target_link_libraries(${TARGET} PUBLIC
  Eigen3::Eigen)
