#ifndef __JOY_MAPPER_H
#define __JOY_MAPPER_H

#include <Eigen/Dense>

#include "joyDrivers.h"
#include "joyStructs.h"
#include "geofencer.h"
#include "buffer.h"

// The JoyMapper class maps joystick button inputs to setpoints in Joy Position
// and Attitude modes.  Position, velocity, and acceleration setpoint values are
// obtain by second-order filtering the raw joystick-commanded velocity.  For
// documentation on the filtering structure, see /doc/filtered_joy_notes.PDF.
class JoyMapper {
 public:
  JoyMapper();
  void config(const JoyParams& p);
  // Initializes internal position setpoint to the input value.  Zeroes internal
  // velocity and acceleration setpoint values.
  void initialize(const Eigen::Vector3d& position_setpoint) {
    pfk_ = position_setpoint;
    vfk_.setZero();
    afk_.setZero();
    eIk_.setZero();
  }
  void joyModeMap(const JoyParams& joy_params, const joyStruct& joy_input,
                  const double& dt, Eigen::Vector3d& position_setpoint,
                  Eigen::Vector3d& velocity_setpoint,
                  Eigen::Vector3d& acceleration_setpoint,
                  Eigen::Vector3d& RPY_setpoint,
                  GeoFencer fence, buffer buff);
  void attitudeModeMap(const JoyParams& joy_params, const joyStruct& joy_input,
                       const double& dt, double& thrust_setpoint,
                       Eigen::Vector3d& RPY_setpoint);

 private:
  // Update position, velocity, and acceleration setpoints.  vk is the raw
  // velocity from joystick.  dt is the update interval in seconds.
  void updateSetpoints(Eigen::Vector3d& vk, const double& dt,const JoyParams& joy_params, GeoFencer fence, buffer buff);

  // Integral of error between raw joystick velocity value and filtered velocity
  // value at time k
  Eigen::Vector3d eIk_;
  // Position, velocity, and acceleration setpoint values at time k
  Eigen::Vector3d pfk_, vfk_, afk_;
  // Horizontal and vertical filter parameters
  double Kh_, ah_, Kv_, av_;
  
};
#endif
