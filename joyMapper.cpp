#include "joyMapper.h"

JoyMapper::JoyMapper() { initialize(Eigen::Vector3d::Zero()); }

void JoyMapper::config(const JoyParams& p) {
  const double omegan_horizontal = 8 * p.Bn_horizontal_Hz * p.zeta_horizontal /
                                   (4 * std::pow(p.zeta_horizontal, 2) + 1);
  Kh_ = 2 * p.zeta_horizontal * omegan_horizontal;
  ah_ = std::pow(omegan_horizontal, 2) / Kh_;

  const double omegan_vertical = 8 * p.Bn_vertical_Hz * p.zeta_vertical /
                                 (4 * std::pow(p.zeta_vertical, 2) + 1);
  Kv_ = 2 * p.zeta_vertical * omegan_vertical;
  av_ = std::pow(omegan_vertical, 2) / Kv_;
}

void JoyMapper::joyModeMap(const JoyParams& joy_params,
                           const joyStruct& joy_input, const double& dt,
                           Eigen::Vector3d& position_setpoint,
                           Eigen::Vector3d& velocity_setpoint,
                           Eigen::Vector3d& acceleration_setpoint,
                           Eigen::Vector3d& RPY_setpoint,
                           GeoFencer fence,
                           buffer buff) {
 
  Eigen::Vector3d velocity_raw; 
  velocity_raw(0) = joy_params.x_rate * joy_input.RstickVer;
  velocity_raw(1) = joy_params.y_rate * joy_input.RstickHor;
  velocity_raw(2) = joy_params.z_rate * joy_input.LstickVer;
 
  updateSetpoints(velocity_raw, dt, joy_params ,fence, buff);

  position_setpoint = pfk_;
  velocity_setpoint = vfk_;
  acceleration_setpoint = afk_;
  const double yaw_dot =
      joy_params.yaw_rate_max * (joy_input.buttonL1 - joy_input.buttonR1);
  RPY_setpoint(0) = 0;
  RPY_setpoint(1) = 0;
  RPY_setpoint(2) += yaw_dot * dt;
}

void JoyMapper::attitudeModeMap(const JoyParams& joy_params,
                                const joyStruct& joy_input, const double& dt,
                                double& thrust_setpoint,
                                Eigen::Vector3d& RPY_setpoint) {
  const double yaw_dot =
      joy_params.yaw_rate_max * (joy_input.buttonL1 - joy_input.buttonR1);
  RPY_setpoint(0) = -joy_params.roll_max * joy_input.RstickHor;
  RPY_setpoint(1) = joy_params.pitch_max * joy_input.RstickVer;
  RPY_setpoint(2) += yaw_dot * dt;
  thrust_setpoint = joy_params.thrust_max * std::max(joy_input.LstickVer, 0.0);
}

void JoyMapper::updateSetpoints(Eigen::Vector3d& vk, const double& dt, const JoyParams& joy_params, GeoFencer fence, buffer buff) {
  // For documentation on the filtering structure, see
  // /doc/filtered_joy_notes.PDF.
  double vel_x, vel_y, vel_z;
  Eigen::Vector3d vel_SF_rot, vel_SF, unrot_max_vel, buffer_vel, unit_buffer_vel;
  unrot_max_vel = {joy_params.x_rate, joy_params.y_rate, joy_params.z_rate};
  Eigen::Vector3d max_vel = fence._RCI*(unrot_max_vel-fence.rot_C)+fence.rot_C;
  Eigen::Vector3d rot_pos = fence._RCI*(pfk_-fence.rot_C)+fence.rot_C;
  Eigen::Vector3d rot_pos_enemy = fence._RCI*(buff.enemy_pos-fence.rot_C)+fence.rot_C;
  buffer_vel = {0,0,0};
  vel_SF = {0,0,0};
  if((fence.violates(rot_pos.x(), fence.x_bounds) || fence.violates(rot_pos.y(), fence.y_bounds)
                                                  || fence.violates(rot_pos.z(), fence.z_bounds)) 
                                                  && fence.geo_flag){
    vel_x = fence.vel_soft_sat(rot_pos.x(), max_vel.x(), fence.x_bounds, fence.X_bounds);
    vel_y = fence.vel_soft_sat(rot_pos.y(), max_vel.y(), fence.y_bounds, fence.Y_bounds);
    vel_z = fence.vel_soft_sat(rot_pos.z(), max_vel.z(), fence.z_bounds, fence.Z_bounds);
    vel_SF = {vel_x, vel_y, vel_z};
  }
  if(buff.violates(pfk_, buff.enemy_pos, buff.long_dist) && buff.buff_flag){
    unit_buffer_vel =  buff.unit_vector(rot_pos, rot_pos_enemy);
    buffer_vel = {max_vel.x()*unit_buffer_vel.x(), max_vel.y()*unit_buffer_vel.y(), max_vel.z()*unit_buffer_vel.z()};
  }
  const Eigen::Vector3d ek = vk  + vel_SF  + buffer_vel -  vfk_;
  afk_(0) = (Kh_ * ek(0) + Kh_ * ah_ * eIk_(0));
  afk_(1) = (Kh_ * ek(1) + Kh_ * ah_ * eIk_(1));
  afk_(2) = (Kv_ * ek(2) + Kv_ * ah_ * eIk_(2));
  vfk_ += dt * afk_;  
  pfk_ += (dt * vfk_ + (0.5 * dt * dt) * afk_);
  eIk_ += dt * ek;  
}
