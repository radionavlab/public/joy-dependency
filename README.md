# joy-dependency

Driver for handling messages on the `/joy` ROS topic. Used by [px4-control](
https://gitlab.com/radionavlab/machine-games/px4-control) for Joy Position and
Attitude modes.
