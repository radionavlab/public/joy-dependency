#include "geofencer.h"

void GeoFencer::initialize(const Eigen::Vector2d bounds_X,
                     const Eigen::Vector2d bounds_Y,
                     const Eigen::Vector2d bounds_Z,
                     const Eigen::Vector2d bounds_x,
                     const Eigen::Vector2d bounds_y,
                     const Eigen::Vector2d bounds_z,
                     const Eigen::Matrix3d RCI,
                     const Eigen::Vector3d rotCenter,
                     const bool GeoFlag) {

    X_bounds = bounds_X;
    Y_bounds = bounds_Y;
    Z_bounds = bounds_Z;
    x_bounds = bounds_x;
    y_bounds = bounds_y;
    z_bounds = bounds_z;
    _RCI = RCI;
    rot_C = rotCenter;
    geo_flag = GeoFlag;
}

void GeoFencer::geofencePVA(Eigen::Vector3d &pos, Eigen::Vector3d &vel, Eigen::Vector3d &acc, double dt){
  bool violatesGeofence_hard = false;
  bool violatesGeofence_soft = false;
  static double Time;
  static bool out_flag;
  static Eigen::Vector3d ini_vel, ini_acc;
  Eigen::Vector3d pos_Co = _RCI*(pos-rot_C)+rot_C;
  Eigen::Vector3d vel_Co = _RCI*(vel-rot_C)+rot_C;
  Eigen::Vector3d acc_Co = _RCI*(acc-rot_C)+rot_C;
  if (violates(pos_Co.x(), X_bounds) ||
      violates(pos_Co.y(), Y_bounds) ||
      violates(pos_Co.z(), Z_bounds) )
  {
    violatesGeofence_hard = true;
  }
  else if (violates(pos_Co.x(), x_bounds) ||
           violates(pos_Co.y(), y_bounds) ||
           violates(pos_Co.z(), z_bounds) )
  {
    violatesGeofence_soft = true;
  }
  else {
    ini_vel = vel_Co;
    ini_acc = acc_Co;
    Time = 0;
  }
  if (violatesGeofence_hard) {
    pos_Co.x() = saturate(pos_Co.x(), X_bounds);
    pos_Co.y() = saturate(pos_Co.y(), Y_bounds);
    pos_Co.z() = saturate(pos_Co.z(), Z_bounds);
    vel = Eigen::Vector3d::Zero();
    acc = Eigen::Vector3d::Zero();
  }
  pos = _RCI.transpose()*(pos_Co-rot_C)+rot_C ;
}

// Apply saturation defined by bounds to a value and return the value.
double GeoFencer::saturate(double val, const Eigen::Vector2d hard_bounds)
{
  double min = hard_bounds(0);
  double max = hard_bounds(1);

  if (val>max)
    return max;
  else if (val<min)
    return min;
  else
    return val;
}

// Apply saturation defined by bounds to a value and return the value.
double GeoFencer::pos_soft_sat(double val, const Eigen::Vector2d soft_bounds, const Eigen::Vector2d hard_bounds, double ini_vel,
                               double time)
{  
  const double wn = 1.0;
  double c1, c2, pos_fin;
  double hard_min = hard_bounds(0); 
  double hard_max = hard_bounds(1);
  double soft_min = soft_bounds(0);
  double soft_max = soft_bounds(1);
  double V = ini_vel*ini_vel;
  if (val>soft_max){
    c1 = soft_max-hard_max;
    c2 = ini_vel + wn*c1;
    pos_fin = soft_max;
  }
  else if (val<soft_min){
    c1 = soft_min-hard_min;
    c2 = (ini_vel + wn*c1);
    pos_fin = soft_min;
  }
  else{
    return val;
  }
  double pos = (c1 + c2*time)*exp(-wn*time) + pos_fin;
  return pos;   
}


// Apply saturation defined by bounds to a value and return the value.
double GeoFencer::vel_soft_sat(double val_pos, double vel_max, const Eigen::Vector2d soft_bounds, const Eigen::Vector2d hard_bounds)
{
   double vel;
   double hard_min = hard_bounds(0); 
   double hard_max = hard_bounds(1);
   double soft_min = soft_bounds(0);
   double soft_max = soft_bounds(1);
   if(val_pos > hard_max){
    vel = -2*vel_max;
   }
   else if(val_pos < hard_min){
    vel = 2*vel_max;
   }
   else if (val_pos < soft_min){
    vel = 2*vel_max*(val_pos-soft_min)/(hard_min-soft_min);
   }
   else if (soft_max < val_pos){
    vel = -2*vel_max*(val_pos-soft_max)/(hard_max-soft_max);
   }
   else {
     vel = 0;
   }
   return vel;   
}

 // Apply saturation defined by bounds to a value and return the value.
 double GeoFencer::acc_soft_sat(double val_pos, const Eigen::Vector2d soft_bounds)
 {
    double acc;
    double soft_min = soft_bounds(0);
    double soft_max = soft_bounds(1);
    if (val_pos>soft_max){
      acc = -6;
    }
    else if (val_pos<soft_min){
      acc = 6;
    }
    else
      acc = 0;
    return acc;  
}


// Checks if position violates bounds. Simple 1d.
double GeoFencer::violates(double val, const Eigen::Vector2d bounds)
{
  double min = bounds(0);
  double max = bounds(1);

  if (val>max || val<min)
    return true;
  else
    return false;
}

